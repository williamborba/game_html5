//<![CDATA[
var animationRequest = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame,
        animationCancel = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.msCancelRequestAnimationFrame || window.oCancelRequestAnimationFrame,
        animationStatus,
        clientWidth,
        clientHeight,
        config,
        player,
        plrPrvX = 0,        
        plrPrvY = 0,
        plrImgX = 0,
        plrImgY = 0,
        map,
        context,
        keyCode,
        eventCtrl;

function load(fn) {
        if (document.addEventListener) {
                return document.addEventListener("DOMContentLoaded",fn,false);
        } else if (document.attachEvent) {
                return document.attachEvent("onreadystatechange",fn);
        }
}
function ready(obj,event,fn) {
        if (obj.addEventListener) {
                return obj.addEventListener(event,fn,false);
        } else if (obj.attachEvent)         {
                return obj.attachEvent(event,fn);
        }
}
function ajax(file,method,params) {
        client = new XMLHttpRequest();
        data = null;
        client.onreadystatechange = function() {
                if (client.readyState == client.DONE) {
                        if(client.status == 200) {
                                data = JSON.parse(client.responseText);
                        }
                }
        };
        client.open(method,file,false);
        switch(method) {
                case "get":
                        client.setRequestHeader("Content-Type", "text/html");
                        break;
                case "post":
                        client.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        break;
        }
        client.send(params);
        return data;
}
function init(cfg) {
        config = cfg;
        eventCtrl = 1;
        context2d();
        loadMap();
        loadPlayer();
        ready(document,"keydown",loadCommands);
}
function loadCommands(event) {
        if (eventCtrl) {
                keyCode = event.keyCode;
                plrPrvX = config.plrX * config.plrSqr;
                plrPrvY = config.plrY * config.plrSqr;
                switch (event.keyCode) {
                        case 37:
                                plrImgY = config.plrImgSze.height;
                                config.plrX -= 1;
                                eventCtrl = 0;
                                movePlayer();
                                break;
                        case 38:
                                plrImgY = config.plrImgSze.height * 3;
                                config.plrY -= 1;
                                eventCtrl = 0;
                                movePlayer();
                                break;
                        case 39:
                                plrImgY = config.plrImgSze.height * 2;
                                config.plrX += 1;
                                eventCtrl = 0;
                                movePlayer();
                                break;
                        case 40:
                                plrImgY = 0;
                                config.plrY += 1;
                                eventCtrl = 0;
                                movePlayer();
                                break;
                }
        }
}
function movePlayer() {
        if (keyCode == 37) {
                if (plrPrvX > (config.plrX * config.plrSqr)) {
                        plrPrvX -= 2;
                        loadMap();
                        movePlayerImg();
                        x = (plrPrvX >= (clientWidth / 2)) ? (clientWidth / 2) : plrPrvX;
                        y = ((plrPrvY + config.plrSqr) >= (clientHeight / 2)) ? (clientHeight / 2 - config.plrSqr) : plrPrvY;
                        context.drawImage(player,plrImgX,plrImgY,config.plrImgSze.width,config.plrImgSze.height,x,y,config.plrImgSze.width,config.plrImgSze.height);
                        animationStatus = animationRequest(movePlayer);
                } else {
                        eventCtrl = 1;
                        return animationCancel(animationStatus);
                }
        }
        if (keyCode == 38) {
                if (plrPrvY > (config.plrY * config.plrSqr)) {
                        plrPrvY -= 2;
                        loadMap();
                        movePlayerImg();
                        x = (plrPrvX >= (clientWidth / 2)) ? (clientWidth / 2) : plrPrvX;
                        y = ((plrPrvY + config.plrSqr) >= (clientHeight / 2)) ? (clientHeight / 2 - config.plrSqr) : plrPrvY;
                        context.drawImage(player,plrImgX,plrImgY,config.plrImgSze.width,config.plrImgSze.height,x,y,config.plrImgSze.width,config.plrImgSze.height);
                        animationStatus = animationRequest(movePlayer);
                } else {
                        eventCtrl = 1;
                        return animationCancel(animationStatus);
                }
        }
        if (keyCode == 39) {
                if (plrPrvX < (config.plrX * config.plrSqr)) {
                        plrPrvX += 2;
                        loadMap();
                        movePlayerImg();
                        x = (plrPrvX >= (clientWidth / 2)) ? (clientWidth / 2) : plrPrvX;
                        y = ((plrPrvY + config.plrSqr) >= (clientHeight / 2)) ? (clientHeight / 2 - config.plrSqr) : plrPrvY;
                        context.drawImage(player,plrImgX,plrImgY,config.plrImgSze.width,config.plrImgSze.height,x,y,config.plrImgSze.width,config.plrImgSze.height);
                        animationStatus = animationRequest(movePlayer);
                } else {
                        eventCtrl = 1;
                        return animationCancel(animationStatus);
                }
        }
        if (keyCode == 40) {
                if (plrPrvY < (config.plrY * config.plrSqr)) {
                        plrPrvY += 2;
                        loadMap();
                        movePlayerImg();
                        x = (plrPrvX >= (clientWidth / 2)) ? (clientWidth / 2) : plrPrvX;
                        y = ((plrPrvY + config.plrSqr) >= (clientHeight / 2)) ? (clientHeight / 2 - config.plrSqr) : plrPrvY;
                        context.drawImage(player,plrImgX,plrImgY,config.plrImgSze.width,config.plrImgSze.height,x,y,config.plrImgSze.width,config.plrImgSze.height);
                        animationStatus = animationRequest(movePlayer);
                } else {
                        eventCtrl = 1;
                        return animationCancel(animationStatus);
                }
        }
}
function movePlayerImg() {
        if (plrPrvX % config.plrSqr == 0 && plrPrvY % config.plrSqr == 0) {
                plrImgX = plrImgX == ((config.plrImgSds - 1) * config.plrImgSze.width) ? 0 : plrImgX += config.plrImgSze.width;
        }
}
function loadPlayer() {
        if (player) {
                context.drawImage(player,0,0,config.plrImgSze.width,config.plrImgSze.height,config.plrX * config.plrSqr,config.plrY * config.plrSqr,config.plrImgSze.width,config.plrImgSze.height);
        } else {
                objPlayer = new Image();
                objPlayer.src = config.plrImg;
                player = objPlayer;
                context.drawImage(player,0,0,config.plrImgSze.width,config.plrImgSze.height,config.plrX * config.plrSqr,config.plrY * config.plrSqr,config.plrImgSze.width,config.plrImgSze.height);
        }
}
function loadMap() {
        if (map) {
                switch (keyCode) {
                        case 37:
                                config.mapX = plrPrvX >= (clientWidth / 2) ? config.mapX - 2 : config.mapX;
                                break;
                        case 38:
                                config.mapY = (plrPrvY + config.plrSqr) >= (clientHeight / 2) ? config.mapY - 2 : config.mapY;
                                break;
                        case 39:
                                config.mapX = plrPrvX >= (clientWidth / 2) ? config.mapX + 2 : config.mapX;
                                break;
                        case 40:
                                config.mapY = (plrPrvY + config.plrSqr) >= (clientHeight / 2) ? config.mapY + 2 : config.mapY;
                                break;
                        default:
                                config.mapX = 0;
                                config.mapY = 0;
                }
                context.drawImage(map,config.mapX,config.mapY,clientWidth,clientHeight,0,0,clientWidth,clientHeight);
        } else {
                map = new Image();
                map.src = config.mapImg;
                context.drawImage(map,0,0);
        }
}
function context2d() {
        innerSet();
        if (context) {
                get("#" + config.ctxTgt).setAttribute("width", clientWidth);
                get("#" + config.ctxTgt).setAttribute("height", clientHeight);
        } else {
                ctxTgt = document.createElement(config.ctxTgt);
                ctxTgt.setAttribute("width", clientWidth);
                ctxTgt.setAttribute("height", clientHeight);
                ctxTgt.setAttribute("id", config.ctxTgt);
                document.body.appendChild(ctxTgt);
                context = ctxTgt.getContext('2d');
        }
}
function innerSet() {
        clientWidth = window.document.body.clientWidth >= clientWidth ? clientWidth : window.document.body.clientWidth;
        clientHeight = window.document.body.clientHeight >= clientHeight ? clientHeight : window.document.body.clientHeight;
}
function each(obj, callback) {
        value = null;
        for(value in obj) {
                callback(obj[value]);
        }
}
function val(obj) {
        list = [];
        for(i = 0; i < obj.length; ++i) {
                list.push(obj[i].value);
        }
        return list;
}
function get(par) {
        re = /^[.#]/;
        if (re.test(par)) {
                return document.querySelector(par);
        } else {
                return document.querySelectorAll(par);
        }
}
function resize(obj, fn) {
        ready(obj, "resize", fn);
}
function click(obj, fn) {
        ready(obj, "click", fn);
}
//]]>
